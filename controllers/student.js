const studentModel = require("../models/student");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

exports.createUser = async (req, res) => {
  const { username, password } = req.body;

  try {
    const userExists = await studentModel.findOne({ where: { username } });

    if (userExists) {
      return res.status(400).json({
        success: false,
        message: "username already exists",
      });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const user = await studentModel.create({
      username,
      password: hashedPassword,
    });

    res.status(200).json({
      success: true,
      message: "User created successfully",
      data: user,
    });
  } catch (error) {
    res.status(500).send({
      success: false,
      message: "Internal Server Error.",
      error: error.message,
    });
  }

  exports.login = async (req, res) => {
    const { username, password } = req.body;
    const user = await studentModel.findOne({ where: { username } });
    console.log(user);
    try {
      if (!username || !password) {
        return res.status(400).send({
          success: false,
          message: "Please provide the necessary fields.",
        });
      }

      if (!user) {
        return res.status(400).send({
          success: false,
          message: "Invalid Credentials.",
        });
      }

      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        return res.status(400).send({
          success: false,
          message: "Invalid Credentials.",
        });
      }
      const jwtToken = jwt.sign({ username: user.username }, "jwt5$%12345ert");

      res.status(200).send({
        success: true,
        message: "Login successful",
        token: jwtToken,
      });
    } catch (error) {
      res.status(500).send({
        success: false,
        message: "Internal Server Error.",
        error: error.message,
      });
    }
  };
};
exports.getSchedules = async (req, res) => {
  const { id } = req.params;

  try {
    const schedule = await scheduleModel.findByPk(id);
    if (schedule) {
      res.status(200).json(schedule);
    } else {
      res.status(404).json({ message: "Schedule not found" });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal server error" });
  }
};

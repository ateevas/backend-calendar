const Sequelize = require("sequelize");
const sequelize = require("../utils/db");

const Schedule = sequelize.define("schedule", {
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  description: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  startDate: {
    type: Sequelize.DATE,
    allowNull: false,
  },

  endDate: {
    type: Sequelize.DATE,
    allowNull: false,
  },
});
const Professor = require("./user");
const Student = require("./student");
Schedule.belongsTo(Professor, { foreignKey: "professor_id" });
Schedule.belongsTo(Student, { foreignKey: "student_id" });
module.exports = Schedule;

const scheduleModel = require("../models/schedule");

exports.createSchedule = async (req, res) => {
  const { title, startDate, endDate, description } = req.body;

  try {
    const schedule = await scheduleModel.create({ title, startDate, endDate, description });
    res.status(201).json(schedule);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal server error" });
  }
};

//get all scheds
exports.getSchedules = async (req, res) => {
  let schedules = await scheduleModel.findAll();
  res.send(schedules);
};

exports.getSchedule = async (req, res) => {
  const { id } = req.params;

  try {
    const schedule = await scheduleModel.findByPk(id);
    if (schedule) {
      res.status(200).json(schedule);
    } else {
      res.status(404).json({ message: "Schedule not found" });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal server error" });
  }
};

exports.deleteSchedule = async (req, res, next) => {
  try {
    const { id } = req.params;
    const schedule = await scheduleModel.findByPk(id);
    if (!schedule) {
      throw createError(404, "schedule not found");
    }
    await schedule.destroy();
    res.json({ message: "schedule deleted successfully" });
  } catch (error) {
    next(error);
  }
};

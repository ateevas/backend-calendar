const Router = require("express");
const studentController = require("../controllers/student");
const router = Router();
router.post("/student", studentController.createUser);

router.get("/student", studentController.getSchedules);
module.exports = router;

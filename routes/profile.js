const Router = require("express");
const profileController = require("../controllers/profile");
const router = Router();

router.post("/profile/:id", profileController.createProfile);
router.get("/profile/:userId", profileController.getProfile);
router.put("/:id", profileController.updateProduct);

module.exports = router;

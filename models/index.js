const sequelize = require("../utils/db");
const scheduleModel = require("./schedule");
const userModel = require("./user");
const profileModel = require("./profile");
const studentModel = require("./student");

scheduleModel.sync();
userModel.sync();
profileModel.sync();
studentModel.sync();

module.exports = sequelize;

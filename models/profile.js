const Sequelize = require("sequelize");
const sequelize = require("../utils/db");

const Profile = sequelize.define("profile", {
  username: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  firstname: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  lastname: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  studentId: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
});

module.exports = Profile;

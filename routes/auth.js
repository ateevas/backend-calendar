const Router = require("express");
const passport = require("passport");
const userController = require("../controllers/user");
const router = Router();
const middleware = require("../middleware/auth");

router.post("/register", userController.createUser);
router.get("/user", middleware, userController.getUser);
router.post("/logout", userController.logout);

router.post("/login", userController.login);

module.exports = router;

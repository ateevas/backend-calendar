const express = require("express");
const createError = require("http-errors");

const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
const authRoute = require("./routes/auth");
const profileRoute = require("./routes/profile");
const scheduleRoute = require("./routes/schedule");
const studentRoute = require("./routes/student");

//const passport = require("passport");

app.use(bodyParser.json());
app.use(cors());
app.use("/auth", authRoute);
app.use("/profile", profileRoute);
app.use("/", scheduleRoute);
app.use("/", studentRoute);
//app.use(passport.initialize());
//app.use(passport.session());

app.use((req, res) => {
  res.send(createError(404));
});
module.exports = app;

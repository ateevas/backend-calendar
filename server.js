const http = require("http");
const app = require("./app.js");
const server = http.createServer(app);

const model = require("./models");

model.sync().then(() => {
  const port = process.env.PORT || 5000;
  server.listen(port, () => console.log("Server is running at http://localhost:" + port));
});

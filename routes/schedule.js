const Router = require("express");
const scheduleController = require("../controllers/schedule");
const router = Router();

router.post("/schedule", scheduleController.createSchedule);
router.get("/schedule/:id", scheduleController.getSchedule);
router.get("/schedules", scheduleController.getSchedules);
router.delete("/schedules/:id", scheduleController.deleteSchedule);

module.exports = router;

const profileModel = require("../models/profile");
const userModel = require("../models/user");

exports.createProfile = async (req, res) => {
  const { body } = req;
  const profileId = req.params.id;

  const profile = await profileModel.create({
    username: body.username,
    firstname: body.firstname,
    lastname: body.lastname,
    studentId: body.studentId,
    userId: profileId,
  });

  res.send(profile);
  {
    console.log(error);
    res.status(500).send("An error occurred while creating the profile.");
  }
};

exports.updateProduct = async (req, res) => {
  const { id } = req.params;
  const { username, firstname, lastname, studentId } = req.body;

  try {
    const product = await userModel.findByPk(id);

    if (!product) {
      return res.status(404).json({ message: "Product not found" });
    }

    await product.update({ username, firstname, lastname, studentId });

    return res.json({ message: "Product name updated successfully" });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal server error" });
  }
};

exports.getProfile = async (req, res) => {
  const { userId } = req.params;

  try {
    const Profile = await profileModel.findOne({ where: { userId } });
    if (Profile) {
      res.status(200).json(Profile);
    } else {
      res.status(404).json({ message: "Profile not found" });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal server error" });
  }
};

// const profileModel = require("../models/profile");

// exports.createProfile = async (req, res) => {
//   const { body } = req;
//   const profileId = req.params.id;
//   const { id } = req.params;
//   try {
//     const profileExists = await profileModel.findOne({ findByPrimarykey: { id } });

//     if (profileExists) {
//       return res.status(400).json({
//         success: false,
//         message: "Profile already exists",
//       });
//     }

//     const profile = await profileModel.create({
//       username: body.username,
//       firstname: body.firstname,
//       lastname: body.lastname,
//       studentId: body.studentId,
//       userId: profileId,
//     });
//     res.send(profile);

//     res.status(200).json({
//       success: true,
//       message: "Profile created successfully",
//       data: user,
//     });
//   } catch (error) {
//     res.status(500).send({
//       success: false,
//       message: "Internal Server Error.",
//       error: error.message,
//     });
//   }
// };

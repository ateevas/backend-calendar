const bcrypt = require("bcrypt");
const userModel = require("../models/user");
const jwt = require("jsonwebtoken");

exports.createUser = async (req, res) => {
  const { username, password, email, firstname, lastname, studentId } = req.body;

  try {
    const userExists = await userModel.findOne({ where: { username } });

    if (userExists) {
      return res.status(400).json({
        success: false,
        message: "username already exists",
      });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const user = await userModel.create({
      username,
      email,
      password: hashedPassword,
      firstname,
      lastname,
      studentId,
    });

    res.status(200).json({
      success: true,
      message: "User created successfully",
      data: user,
    });
  } catch (error) {
    res.status(500).send({
      success: false,
      message: "Internal Server Error.",
      error: error.message,
    });
  }
};

exports.login = async (req, res) => {
  const { username, password } = req.body;
  const user = await userModel.findOne({ where: { username } });
  console.log(user);
  try {
    if (!username || !password) {
      return res.status(400).send({
        success: false,
        message: "Please provide the necessary fields.",
      });
    }

    if (!user) {
      return res.status(400).send({
        success: false,
        message: "Invalid Credentials.",
      });
    }

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).send({
        success: false,
        message: "Invalid Credentials.",
      });
    }
    const jwtToken = jwt.sign({ username: user.username, id: user.id }, "jwt5$%12345ert");

    res.status(200).send({
      success: true,
      message: "Login successful",
      token: jwtToken,
    });
  } catch (error) {
    res.status(500).send({
      success: false,
      message: "Internal Server Error.",
      error: error.message,
    });
  }
};
exports.getUser = async (req, res) => {
  try {
    const user = await userModel.findByPk(req.userId);
    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).json({ message: "user not found" });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal server error" });
  }
};

exports.user = async (req, res) => {
  const user = await UserModel.findAll();
  res.send(user);
};
exports.logout = async (req, res) => {
  try {
    res.clearCookie("token");
    res.status(200).send({
      success: true,
      message: "Logout successful",
    });
  } catch (error) {
    res.status(500).send({
      success: false,
      message: "Internal Server Error.",
      error: error.message,
    });
  }
};

const Sequelize = require("sequelize");

const sequelize = new Sequelize({
  hostname: "localhost",
  database: "calendar",
  username: "root",
  password: null,
  dialect: "mysql",
});

module.exports = sequelize;

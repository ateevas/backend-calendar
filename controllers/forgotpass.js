// //Forgot Password
// const nodemailer = require('nodemailer');
// const StudentModel = require('../models/student')
// const ProfessorModel = require('../models/user')
// const crypto = require('crypto');
// const bcrypt = require('bcrypt')

// exports.forgotPassword = async (req, res) => {
//   const { email } = req.body;
//   let user = await ProfessorModel.findOne({ where: { email: email } }); // find the user by their email

//   if (!user) {
//     user = await StudentModel.findOne({ where: { email: email } }); // if email not found in ProfessorModel, search in StudentModel
//     if (!user) {

//       return res.status(404).json({ message: 'User not found.' });
//     }
//   }

//   // Generate new password
//   const newPassword = crypto.randomBytes(4).toString('hex');
//   const hashedPassword = await bcrypt.hash(newPassword, 10);
//   // Update user's password
//   user.password = hashedPassword;
//   await user.save();

//   // Send email with new password
//   const transporter = nodemailer.createTransport({
//     service: 'Gmail',
//     auth: {
//       user: process.env.GMAIL_EMAIL,
//       pass: process.env.GMAIL_PASSWORD,
//     },
//   });

//   const mailOptions = {
//     from: process.env.EMAIL_USER,
//     to: email,
//     subject: 'Password reset',
//     text: Your new password is: ${newPassword},
//   };

//   transporter.sendMail(mailOptions, (error, info) => {
//     if (error) {
//       console.log(error);
//       res.status(500).json({ message: 'Failed to send email.' });
//     } else {
//       console.log('Email sent: ' + info.response);
//       res.json({ message: 'Email sent.' });
//     }
//   });
// };
